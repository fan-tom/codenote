"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib import admin
from django.views.generic.edit import CreateView
from myapp.views import *
from myapp.forms import AuthorCreate

urlpatterns = [
    url(r'^login$', LoginView.as_view(), name='login'),
    url(r'^logout$', Logout.as_view(), name='logout'),
    # url(r'^signup$', 'django.contrib.admin.views.)
    # url('^signup$', AuthorCreate.as_view(), name='signup'),
    url(r'^signup$',
        CreateView.as_view(template_name="signup.html", form_class=AuthorCreate, success_url=settings.LOGIN_URL),
        name='signup'),
    url(r'^admin/', admin.site.urls, name='admin'),
    url(r'^note/(?P<pk>\d+)$', NoteDetail.as_view(), name='note_detail'),
    url(r'^note/create$', NoteCreate.as_view(), name='note_create'),
    url(r'^note/(?P<pk>\d+)/comment$', CommentCreate.as_view(), name='comment'),
    url(r'^note/(?P<pk>\d+)/like$', Like.as_view(), name='like'),
    url(r'^author/(?P<pk>\d+)/profile', AuthorProfile.as_view(), name='profile'),
    url(r'^$', NotesList.as_view(), name='home')
]
