/**
 * Created by fantom on 08.06.16.
 */
$(document).ready(function () {
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });

    $("span.likes>button").click(function () {
        var pk = $(location).attr('pathname').match(/note\/(\d+)/)[1];
        var attitude = $(this).hasClass('like') ? 'like' : 'dislike';
        var current = $(this);
        $.ajax({
            url: "/note/" + pk + "/like",
            type: "POST",
            data: {attitude: attitude},

            success: function (json) {
                if (json && json.result !== 'error' && pk) {
                    if (json.result !== 'unmodified') {
                        console.log(json.count);
                        current.children("text").text(json.count);
                        if (json.result === 'changed') {
                            var opposite_counter = current.siblings("button").children("text");
                            opposite_counter.text(parseInt(opposite_counter.text()) - 1);
                        }

                    }
                    else {
                        var m = 'unmodified';
                        console.log(m);
                        alert(m);
                    }
                } else {
                    var m = json && json.message ? json.message : 'Error occurred during request!';
                    console.log(m);
                    alert(m);
                }
            },

            error: function (xhr, errmsg, err) {
                console.log('Error occurred during request!' + errmsg)
            }
        });
    });
});
