from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class Author(models.Model):
    user = models.OneToOneField(User, default=1)
    created_at = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('profile', kwargs={"pk": self.id})

class Assessment(models.Model):
    author = models.ForeignKey(Author)
    # like or dislike
    attitude = models.BooleanField()

    class Meta:
        unique_together = (('author', 'attitude'),)


class Note(models.Model):
    title = models.CharField(max_length=255)
    code = models.TextField()
    description = models.TextField()
    time = models.DateTimeField(auto_now=True)
    # json comma-separated tags
    keywords = models.TextField()
    author = models.ForeignKey(Author)
    assessments = models.ManyToManyField(Assessment)

    def get_likes(self):
        return str(self.assessments.filter(attitude=True).count())

    def get_dislikes(self):
        return str(self.assessments.filter(attitude=False).count())

class Comment(models.Model):
    text = models.TextField()
    author = models.ForeignKey(Author)
    note = models.ForeignKey(Note)
    assessments = models.ManyToManyField(Assessment)
