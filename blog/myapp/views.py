import json
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, FormView, CreateView, RedirectView, DetailView
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.contrib.auth.models import *
from django.db.models import Q
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from .models import Note, Author, Comment, Assessment
from .forms import Login, NoteCreate, CommentCreate
from myapp import forms

import logging

logger = logging.getLogger('title')


class AuthorCreate(CreateView):
    form_class = UserCreationForm
    template_name = 'signup.html'
    model = Author


class AuthorProfile(DetailView):
    model = Author
    template_name = 'myapp/author_profile.html'


class LoginView(FormView):
    form_class = Login
    success_url = reverse_lazy('home')
    template_name = 'login.html'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None and user.is_active:
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)
            # def get(self, request, *args, **kwargs):
            #     next=request.GET['next']
            #
            # def get_context_data(self, **kwargs):
            #     context=super(FormView,self).
            #
            # def post(self, request, *args, **kwargs):
            #     redirect=request.POST['next']


def addTitle(get_title):
    def decorator(func):
        def wrapper(*args, **kwarg):
            context = func(*args, **kwarg)
            context['title'] = get_title()
            logger.debug(context['title'])
            return context

        return wrapper

    return decorator


class Logout(RedirectView):
    url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super().get(request, *args, **kwargs)


class NotesList(ListView):
    model = Note
    title = 'Notes'

    def get_title(self):
        def actual():
            return self.title

        return actual

    # @method_decorator(addTitle(get_title()))
    def get_context_data(self, **kwargs):
        # logger.debug(self.title)
        context = super().get_context_data(**kwargs)
        context['title'] = self.title
        return context

    def get_queryset(self):
        notes = super().get_queryset()
        # notes = Note.objects.all()
        query = self.request.GET.get('q')
        keyword = self.request.GET.get('kw')
        if query:
            notes = notes.filter(Q(title__icontains=query))
            self.title = 'Search results for "' + query + '"'
        elif keyword:
            notes = notes.filter(Q(keywords__regex=r'\b' + keyword + r'\b'))
            self.title = 'Notes that tagged by "' + keyword + '"'

        return notes


class NoteCreate(LoginRequiredMixin, CreateView):
    # redirect_field_name = ''
    form_class = NoteCreate
    template_name = 'myapp/note_create.html'
    model = Note
    success_url = '/'

    def form_valid(self, form):
        form.instance.author = Author.objects.get(user=self.request.user)
        return super().form_valid(form)


class NoteDetail(DetailView):
    model = Note
    template_name = 'myapp/note_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comment_form'] = forms.CommentCreate
        context['title'] = self.object.title  # super().get_object().title
        # context['comments'] = Comment.objects.all().filter(note=self.object.pk)
        context['likes'] = self.object.assessments.filter(attitude=True)
        context['dislikes'] = self.object.assessments.filter(attitude=False)
        context['keywords'] = self.object.keywords.split(',')
        return context


class Like(LoginRequiredMixin, CreateView):
    model = Assessment

    def post(self, request, *args, **kwargs):
        result = 'added'
        try:
            # pk = int(request.POST.get('pk'))
            pk = self.kwargs['pk']
            note = get_object_or_404(Note, pk=pk)
            attitude = request.POST.get('attitude')
            if attitude == 'like':
                bool_attitude = True
            elif attitude == 'dislike':
                bool_attitude = False
            else:
                return HttpResponse(json.dumps({'result': 'error', 'message': "Invalid assessment type:" + attitude}),
                                    content_type="application/json")
            assessments = note.assessments
            same_attitude_assessments = assessments.filter(attitude=bool_attitude)
            author_assessments = assessments.filter(author=request.user.author)

            if author_assessments.filter(attitude=bool_attitude).exists():
                return HttpResponse(json.dumps({'result': 'unmodified'}), content_type="application/json")

            opposite_assessment = author_assessments.filter(attitude=not bool_attitude)
            if opposite_assessment.exists():
                note.assessments.remove(opposite_assessment.first())
                result = 'changed'
            assessment, _ = Assessment.objects.get_or_create(author=request.user.author, attitude=bool_attitude)
            # assessment.save()
            note.assessments.add(assessment)
            return HttpResponse(json.dumps({'result': result, 'count': same_attitude_assessments.count()}),
                                content_type="application/json")
        except ValueError:
            return HttpResponse(json.dumps({'result': 'error', 'message': "Invalid issue id!"}),
                                content_type="application/json")


class CommentCreate(LoginRequiredMixin, CreateView):
    form_class = CommentCreate
    # template_name = 'comment_create.html'
    model = Comment

    def get_success_url(self):
        return reverse('note_detail', kwargs={"pk": self.kwargs['pk']})

    def form_valid(self, form):
        form.instance.author = Author.objects.get(user=self.request.user)
        form.instance.note = Note.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)
        # return HttpResponseRedirect('/')

# Create your views here.
