# from django import forms
# from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
# from django.contrib.auth.models import User
# from .models import Author
#
#
# class AuthorCreate(UserCreationForm):
#         first_name=forms.CharField(required=False)
#         last_name=forms.CharField(required=False)
#         username=forms.CharField(required=True)
#         password1 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={"palceholder":"Password"}))
#         password2 = forms.CharField(required=True, widget=forms.PasswordInput(attrs={"palceholder":"Confirm password"}))
#
#         def save(self):
#                 user=super(AuthorCreate, self).save()
#                 author=Author(nick_name=user)
#                 author.save()
#                 return author
#         class Meta:
#             model=User
#
# class Login(AuthenticationForm):
#         def __init__(self, *args, **kwargs):
#                 super(Login,self).__init__(*args,**kwargs)

from django.shortcuts import redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from django import forms
from django.forms.models import model_to_dict, fields_for_model
from django.utils.html import strip_tags
from .models import Author, Note, Comment


class AuthorCreate(UserCreationForm):
    first_name = forms.CharField(required=True,
                                 widget=forms.TextInput(attrs={'placeholder': 'First Name', 'class': 'form-control'}))
    last_name = forms.CharField(required=True,
                                widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': 'form-control'}))
    username = forms.CharField(required=True,
                               widget=forms.TextInput(attrs={'placeholder': 'Username', 'class': 'form-control'}))
    password1 = forms.CharField(label="Password", required=True,
                                widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': 'form-control'}))
    password2 = forms.CharField(label="Password Again", required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Password Confirmation', 'class': 'form-control'}))
    email = forms.CharField(required=True,
                            widget=forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'}))

    class Meta:
        fields = ['first_name', 'last_name', 'email', 'username', 'password1',
                  'password2']
        model = User

    def is_valid(self):
        form = super(AuthorCreate, self).is_valid()
        for f, error in self.errors.items():
            if f != '__all__':
                self.fields[f].widget.attrs['data-toggle'] = 'tooltip'
                self.fields[f].widget.attrs['data-placement'] = 'right'
                self.fields[f].widget.attrs['title'] = error[0]

        return form

    def save(self):
        user = super(AuthorCreate, self).save()
        a = Author(user=user)
        a.save()
        return a


class Login(AuthenticationForm):
    username = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Username', 'class': 'form-control login-field'}))
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Password', 'class': 'form-control login-field'}))

    def is_valid(self):
        form = super(AuthenticationForm, self).is_valid()
        for f, error in self.errors.items():
            if f != '__all__':
                self.fields[f].widget.attrs['data-toggle'] = 'tooltip'
                self.fields[f].widget.attrs['data-placement'] = 'right'
                self.fields[f].widget.attrs['title'] = error[0]
        return form


class NoteCreate(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['title', 'code', 'description', 'keywords']


class CommentCreate(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']
